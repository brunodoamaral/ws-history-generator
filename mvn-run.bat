@echo off
@echo This batch will use maven to compile and run the project. If everithing went allright
@echo a list of current buses should be printed on the screen. The program runs continously
@echo gathering newer data until it is forced to stop.
@echo This procedure requires Java 8 and Maven 3.
pause

mvn compile exec:java -Dexec.mainClass="br.com.bguberfain.MainRioBus"
