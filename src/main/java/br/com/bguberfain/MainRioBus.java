package br.com.bguberfain;

import br.com.bguberfain.history.HistoryGenerator;
import br.com.bguberfain.output.DailyRollingFileOutput;
import br.com.bguberfain.output.StandardOuput;
import br.com.bguberfain.services.riobuses.RioBusServicesFactory;

import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Bruno on 07/06/2015.
 * This class uses the 'RioBus' implementation of the services, created from RioBusServicesFactory
 */
public class MainRioBus {
    /**
     * Main method to start the history generation
     *
     * @param args          ignored
     * @throws IOException  if an IO error occurs
     */
    public static void main(String args[]) throws IOException {
        if (args.length != 1) {
            System.err.println("Must specify an output folder");
        } else {

            // Create and configure runner for regular buses
            HistoryGenerator runner = new HistoryGenerator(
                    new RioBusServicesFactory(false),               // Use RioBus's ServicesFactory to build Services
                    30000,                                          // Wait 30s between executions
                    new DailyRollingFileOutput(args[0], ".txt")     // Output to Daily-rolling file
            );

            // Start process
            runner.run(true, true, "regular-buses");

            // Create and configure runner for BRT buses
            HistoryGenerator runnerBRT = new HistoryGenerator(
                    new RioBusServicesFactory(true),               // Use RioBus's ServicesFactory to build Services
                    30000,                                          // Wait 30s between executions
                    new DailyRollingFileOutput(args[0], "-brt.txt")     // Output to Daily-rolling file
            );

            // Start process
            runnerBRT.run(true, true, "brt");
        }
    }


}
