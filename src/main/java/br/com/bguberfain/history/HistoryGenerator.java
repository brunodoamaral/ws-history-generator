package br.com.bguberfain.history;

import br.com.bguberfain.services.Service;
import br.com.bguberfain.services.ServiceState;
import br.com.bguberfain.services.ServiceStateOutput;
import br.com.bguberfain.services.ServicesFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.Writer;
import java.util.Optional;

/**
 * Created by Bruno on 18/04/2015.
 *
 * Class that continuously generates the difference between ServiceState(s) and writerOutput it to ServiceStateOutput.
 *
 */
public class HistoryGenerator {
    private static final Logger logger = LogManager.getLogger(HistoryGenerator.class);

    private final ServicesFactory servicesFactory ;
    private final int waitInterval ;
    private final Writer writerOutput;

    /**
     * Instantiates the HistoryGenerator with its configuration
     *
     * @param servicesFactory   ServicesFactory to build Services
     * @param waitInterval      Interval between calling service.getCurrentState()
     * @param writerOutput            Where to write writerOutput
     */
    public HistoryGenerator(ServicesFactory servicesFactory, int waitInterval, Writer writerOutput) {
        this.servicesFactory = servicesFactory;
        this.waitInterval = waitInterval ;
        this.writerOutput = writerOutput;
    }

    /**
     * Starts the execution of this class. This begins with a state and every 'waitInterval' gather a new state, that
     * should be subtracted from the last one to create a difference of states. This difference is than write to ServiceStateOutput
     *
     * @param runForever    whatever this task should run forever
     * @param taskName
     * @throws IOException  if an IO error occours
     */
    public void run(boolean runForever, boolean runAsDaemon, String taskName) throws IOException  {
        Runnable task = () -> {
            try {
                // Prepare service and writerOutput
                Service service = servicesFactory.createService() ;
                ServiceStateOutput output = servicesFactory.createServiceStateOutput(writerOutput);

                // Ensure read the first state
                Optional<ServiceState> lastState ;
                do {
                    lastState = service.getCurrentState() ;
                } while(!lastState.isPresent()) ;

                // Write the first state to the writerOutput
                output.appendState(lastState.get());

                // Begin main loop
                do {
                    // Wait
                    if (waitInterval > 0) {
                        try {
                            logger.debug("Waiting " + waitInterval + "ms");
                            Thread.sleep(waitInterval);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            logger.error("Error while waiting", e);
                        }
                    }

                    // Gather new state
                    logger.debug("Reading new state...");
                    Optional<ServiceState> currentState = service.getCurrentState() ;
                    if (currentState.isPresent()) {
                        // Calculate the difference of states
                        ServiceState differenceSinceLast = currentState.get().minus(lastState.get());
                        logger.debug("Difference state calculated. Appending to output.");

                        // Write this state to writerOutput
                        output.appendState(differenceSinceLast);

                        // Update lastState to next loop interaction
                        lastState = currentState;
                    } else {
                        logger.debug("No state present");
                    }

                    // TODO: add a graceful way to stop this program
                } while(runForever) ;

                logger.info("Finishing process");
            } catch (IOException e) {
                logger.error("Error in main loop", e);
            }
        } ;

        if (runAsDaemon) {
            new Thread(task, taskName).start();
        } else {
            task.run();
        }
    }

}
