package br.com.bguberfain.output;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Bruno on 07/06/2015.
 */
public class DailyRollingFileOutput extends Writer {
    private static final Logger logger = LogManager.getLogger(DailyRollingFileOutput.class);
    
    private static final SimpleDateFormat dateFormatFile ;
    static {
        dateFormatFile = new SimpleDateFormat("yyyyMMdd") ;
        dateFormatFile.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
    }
    String currentFileName = null ;
    final String fileNameSuffix;
    Writer currentWriter = null ;
    File folder ;

    public DailyRollingFileOutput(String folderOutput, String fileNameSuffix) throws IOException {
        this.fileNameSuffix = fileNameSuffix;
        folder = new File(folderOutput) ;
        if (!folder.exists()) {
            throw new FileNotFoundException("Folder does not exists: " + folderOutput) ;
        } else if (!folder.isDirectory()) {
            throw new IOException("Specified folder is not an actual folder: " + folderOutput) ;
        }
    }

    String getFileNameForDate(Date date) {
        return dateFormatFile.format(date) + fileNameSuffix ;
    }

    Writer getWriterForDate(Date date) throws IOException {
        String fileNameForDate = getFileNameForDate(date) ;
        if ( !fileNameForDate.equals(currentFileName) ) {
            // Close previously opened file
            if (currentWriter != null) {
                logger.info("Closing previously writer");
                currentWriter.flush() ;
                currentWriter.close();
            }
            logger.info("Creating/Opening file " + fileNameForDate);
            currentFileName = fileNameForDate ;
            currentWriter = new BufferedWriter(new FileWriter(new File(folder, currentFileName), true)) ; // true = append
        }
        return currentWriter ;
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        getWriterForDate(new Date()).write(cbuf, off, len);
    }

    @Override
    public void flush() throws IOException {
        if(currentWriter != null) {
            currentWriter.flush();
        }
    }

    @Override
    public void close() throws IOException {
        if(currentWriter != null) {
            currentWriter.close();
        }
    }
}
