package br.com.bguberfain.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Bruno on 18/04/2015.
 *
 * This class represents an abstract Service that provides states as a collection (specifically a Set)
 */
public class CollectionServiceState<T extends Comparable> extends TreeSet<T> implements ServiceState
{
    private static final Logger logger = LogManager.getLogger(CollectionServiceState.class);

    public CollectionServiceState() {
    }

    public CollectionServiceState(Set<T> state) {
        super(state);
    }

    public ServiceState minus(ServiceState otherState) {
        if(otherState instanceof CollectionServiceState) {
            CollectionServiceState<T> diffState = new CollectionServiceState<>(this);
            diffState.removeAll((CollectionServiceState)otherState);

            logger.debug("Minus operation: " + this.size() + " (new state) - " + ((CollectionServiceState) otherState).size() + " (old state) = " + diffState.size());

            return diffState;
        } else {
            throw new UnsupportedOperationException("Cannot process minus of class " + otherState.getClass().getName());
        }
    }
}
