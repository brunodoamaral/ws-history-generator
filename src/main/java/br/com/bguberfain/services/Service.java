package br.com.bguberfain.services;

import java.util.Optional;

/**
 * Created by Bruno on 18/04/2015.
 *
 * This interface represents a service that holds a current state of a system.
 *
 */
public interface Service<T extends ServiceState>
{
    /**
     * This method is used to get a current state of the system
     *
     * @return the current state or null if no state is available
     */
    Optional<T> getCurrentState() ;
}
