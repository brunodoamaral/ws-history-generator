package br.com.bguberfain.services;

/**
 * Created by Bruno on 18/04/2015.
 *
 * Represents a service state, that must be able to be differentiable
 *
 */
public interface ServiceState {

    /**
     * Generates a new ServiceState, which is the difference of this
     *
     * @param otherState    The other state from which this must be subtracted
     * @return              A new instance of ServiceState equivalent to this-otherSate
     */
    ServiceState minus(ServiceState otherState) ;
}
