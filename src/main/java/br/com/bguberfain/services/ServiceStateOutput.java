package br.com.bguberfain.services;

import java.io.IOException;

/**
 * Created by Bruno on 19/04/2015.
 *
 * This interface should be implemented by any class that provides output from ServiceState
 * Examples: output to System.out or a database
 */
public interface ServiceStateOutput {
    /**
     * Append a state to the current output
     * @param state the state to be appended
     * @throws IOException if an io error occurs
     */
    void appendState(ServiceState state) throws IOException;
}
