package br.com.bguberfain.services;

import java.io.Writer;

/**
 * Created by Bruno on 06/06/2015.
 *
 * Factory of services
 *
 */
public interface ServicesFactory {

    /**
     * Creates a Service implementation
     */
    Service createService();

    /**
     * Creates a ServiceStateOutput implementation
     * @param writer    where to write the output
     */
    ServiceStateOutput createServiceStateOutput(Writer writer) ;
}
