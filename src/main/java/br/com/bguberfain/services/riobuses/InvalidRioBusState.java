package br.com.bguberfain.services.riobuses;

/**
 * Created by Bruno on 18/04/2015.
 * Exception thrown when an invalid field is detected on class RioBusSample
 */
class InvalidRioBusState extends Exception {

    /**
     * Creates a new exception
     *
     * @param msg   Message describing this exception
     */
    public InvalidRioBusState(String msg) {
        super(msg) ;
    }
}
