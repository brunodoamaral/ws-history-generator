package br.com.bguberfain.services.riobuses;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Optional;

/**
 * Created by Bruno on 06/06/2015.
 *
 * Gather information from "http://dadosabertos.rio.rj.gov.br/apiTransporte/apresentacao/csv/onibus.cfm".
 * This class was created mostly for test-proposes
 */
public class RioBusRemoteDataProvider {
    private static final Logger logger = LogManager.getLogger(RioBusRemoteDataProvider.class);

//    private final static String RIO_BUS_URL = "http://dadosabertos.rio.rj.gov.br/apiTransporte/apresentacao/csv/onibus.cfm" ;
    private final String serviceUrl ;
    private final CloseableHttpClient httpClient;

    /**
     * Creates a new RioBusRemoteDataProvider
     * @param httpClient    Apache's HttpClient to be used when gathering information
     */
    public RioBusRemoteDataProvider(String serviceUrl, CloseableHttpClient httpClient) {
        this.httpClient = httpClient ;
        this.serviceUrl = serviceUrl ;
    }

    /**
     * Gather a reader from the server. Return "empty" on error.
     * @return  the reader from the server
     */
    public Optional<Reader> remoteStateReader() {
        try {
            logger.debug("Reading from " + serviceUrl);
            HttpGet requestData = new HttpGet(serviceUrl);

            CloseableHttpResponse wsResponse = httpClient.execute(requestData);
            logger.debug("Server result was " + wsResponse.getStatusLine().getStatusCode());
            // Check for OK response (status HTTP 200)
            if ( wsResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK ) {
                InputStream stream = wsResponse.getEntity().getContent() ;
                Reader reader = new InputStreamReader(stream) ;
                return Optional.of(reader) ;
            } else {
                EntityUtils.consumeQuietly(wsResponse.getEntity());
            }
        } catch (Exception e) {
            // Any connection exception should be treated as an "empty" state
            logger.error("Error while reading URL", e);
        }

        logger.debug("No state found -> returning empty");
        return Optional.empty() ;
    }
}
