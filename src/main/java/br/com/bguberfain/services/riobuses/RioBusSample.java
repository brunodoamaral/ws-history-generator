package br.com.bguberfain.services.riobuses;

import java.util.Date;

/**
 * Created by Bruno on 18/04/2015.
 *
 * This class contains a state representation for one bus
 */
public class RioBusSample implements Comparable<RioBusSample> {

    private Date timestamp ;
    private String order ;
    private String lineNumber ;
    private double latitude ;
    private double longitude ;
    private float speed ;
    private long delay ;

    /**
     * Constructor for a bus state
     *
     * @param timestamp     Timestamp of state
     * @param order         Bus order code. Must no be empty or null
     * @param lineNumber    Line number
     * @param latitude      Latitude coordinate in range [-90,+90]
     * @param longitude     Longitude coordinate in range  [-180,+180]
     * @param speed         Instant speed
     * @param delay         Delay of this state, regarding the time it was read
     * @throws InvalidRioBusState   If any invalid field is detected
     */
    public RioBusSample(Date timestamp, String order, String lineNumber, double latitude, double longitude, float speed, long delay) throws InvalidRioBusState {
        // Validate order
        if ( order == null || "".equals(order) ) {
            throw new InvalidRioBusState("Order field must not be null or empty") ;
        }

        // Validate latitude
        if( latitude < -90 || latitude > 90 ) {
            throw new InvalidRioBusState("Latitude must be on range [-90,+90]") ;
        }

        // Validate longitude
        if( longitude < -180 || longitude > 180 ) {
            throw new InvalidRioBusState("Longitude must be on range [-180,+180]") ;
        }

        this.timestamp = timestamp;
        this.order = order;
        this.lineNumber = lineNumber;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.delay = delay;
    }

    /*
     * Getters
     */

    public Date getTimestamp() {
        return timestamp;
    }

    public String getOrder() {
        return order;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getSpeed() {
        return speed;
    }

    public long getDelay() {
        return delay;
    }

    @Override
    public int compareTo(RioBusSample that) {
        if (this.timestamp.compareTo(that.timestamp) < 0) {
            return -1;
        } else if (this.timestamp.compareTo(that.timestamp) > 0) {
            return 1;
        }

        if (this.order.compareTo(that.order) < 0) {
            return -1;
        } else if (this.order.compareTo(that.order) > 0) {
            return 1;
        }

        if (this.lineNumber.compareTo(that.lineNumber) < 0) {
            return -1;
        } else if (this.lineNumber.compareTo(that.lineNumber) > 0) {
            return 1;
        }

        if (this.latitude < that.latitude) {
            return -1;
        } else if (this.latitude > that.latitude) {
            return 1;
        }

        if (this.longitude < that.longitude) {
            return -1;
        } else if (this.longitude > that.longitude) {
            return 1;
        }

        if (this.speed < that.speed) {
            return -1;
        } else if (this.speed > that.speed) {
            return 1;
        }

        return 0;
    }


    /*
     * equals, hashCode and toString: uses all fields, except 'delay'
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RioBusSample busSample = (RioBusSample) o;

        if (Double.compare(busSample.latitude, latitude) != 0) return false;
        if (Double.compare(busSample.longitude, longitude) != 0) return false;
        if (Float.compare(busSample.speed, speed) != 0) return false;
        if (timestamp != null ? !timestamp.equals(busSample.timestamp) : busSample.timestamp != null) return false;
        if (order != null ? !order.equals(busSample.order) : busSample.order != null) return false;
        return lineNumber.equals(busSample.lineNumber);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = timestamp != null ? timestamp.hashCode() : 0;
        result = 31 * result + (order != null ? order.hashCode() : 0);
        result = 31 * result + lineNumber.hashCode();
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (speed != +0.0f ? Float.floatToIntBits(speed) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RioBusSample{" +
                "timestamp=" + timestamp +
                ", order='" + order + '\'' +
                ", lineNumber='" + lineNumber + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", speed=" + speed +
                ", delay=" + delay +
                '}';
    }
}
