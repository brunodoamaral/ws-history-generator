package br.com.bguberfain.services.riobuses;

import br.com.bguberfain.services.CollectionServiceState;
import br.com.bguberfain.services.Service;
import com.csvreader.CsvReader;

import java.io.IOException;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Bruno on 18/04/2015.
 */
public class RioBusService implements Service<CollectionServiceState<RioBusSample>> {

    private final RioBusRemoteDataProvider remoteDataProvider ;
    private final DateFormat wsDateFormat ;
    private final TimeZone timeZoneBrazil;

    public RioBusService(RioBusRemoteDataProvider remoteDataProvider) {
        this.remoteDataProvider = remoteDataProvider ;
        timeZoneBrazil = TimeZone.getTimeZone("America/Sao_Paulo") ;
        wsDateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss") ;
        wsDateFormat.setTimeZone(timeZoneBrazil);
        wsDateFormat.setLenient(false);
    }

    public Optional<CollectionServiceState<RioBusSample>> getCurrentState() {
        try {
            Optional<Reader> serverResult = remoteDataProvider.remoteStateReader() ;

            if (serverResult.isPresent()) {
                // Create Reader from response
                Reader readerStatus = serverResult.get() ;
                CsvReader csvReader = new CsvReader(readerStatus) ;

                // Create collection to hold result
                CollectionServiceState<RioBusSample> result = new CollectionServiceState<>();

                try {
                    // Get current time in pt-BR to calculate delay
                    Date now = Calendar.getInstance(timeZoneBrazil).getTime();

                    // Begin parsing (CSV-format)
                    csvReader.readHeaders();
                    while (csvReader.readRecord()) {
                        try {
                            // Read fields
                            Date timestamp = wsDateFormat.parse(csvReader.get("dataHora"));
                            String order = csvReader.get("ordem");
                            String lineNumber = csvReader.get("linha");
                            double latitude = Double.parseDouble(csvReader.get("latitude"));
                            double longitude = Double.parseDouble(csvReader.get("longitude"));
                            float speed = Float.parseFloat(csvReader.get("velocidade"));

                            // Compute delay
                            int delay = (int) (now.getTime() - timestamp.getTime()) / 1000; // In seconds

                            // Create RioBusSample with fields
                            RioBusSample busSample = new RioBusSample(timestamp, order, lineNumber, latitude, longitude, speed, delay);

                            // Add to result collection
                            result.add(busSample);
                        } catch (ParseException | InvalidRioBusState e) {
                            e.printStackTrace();
                        }
                    }
                } finally {
                    csvReader.close();
                }

                // Return a ServiceState based on 'result'
                if (result.isEmpty()) {
                    return Optional.empty() ;
                } else {
                    return Optional.of(result);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
