package br.com.bguberfain.services.riobuses;

import br.com.bguberfain.services.CollectionServiceState;
import br.com.bguberfain.services.ServiceState;
import br.com.bguberfain.services.ServiceStateOutput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Bruno on 19/04/2015.
 */
public class RioBusServiceStateOutput implements ServiceStateOutput {
    private static final Logger logger = LogManager.getLogger(RioBusServiceStateOutput.class);

    private final Writer writer ;
    private static final Locale OUT_LOCALE = Locale.ENGLISH ;

    private final SimpleDateFormat outDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss") ;

    public RioBusServiceStateOutput(Writer writer) {
        this.writer = writer ;
    }


    @Override
    public void appendState(ServiceState state) throws IOException {
        CollectionServiceState<RioBusSample> samples = (CollectionServiceState<RioBusSample>) state ;

        logger.debug("Appending " + samples.size() + " samples to output");

        for (RioBusSample sample : samples) {
            String lineFormat = String.format(OUT_LOCALE, "%s\t%s\t%s\t%.6f\t%.6f\t%.1f\t%d\n", outDateFormat.format(sample.getTimestamp()), sample.getOrder(), sample.getLineNumber(),
                    sample.getLatitude(), sample.getLongitude(), sample.getSpeed(), sample.getDelay()) ;
            writer.write(lineFormat);
        }
        writer.flush();
    }
}
