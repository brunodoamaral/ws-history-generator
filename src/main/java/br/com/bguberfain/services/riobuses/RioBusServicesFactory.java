package br.com.bguberfain.services.riobuses;

import br.com.bguberfain.services.Service;
import br.com.bguberfain.services.ServiceStateOutput;
import br.com.bguberfain.services.ServicesFactory;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.Writer;

/**
 * Created by Bruno on 06/06/2015.
 */
public class RioBusServicesFactory implements ServicesFactory {
    private final static String RIO_BUS_URL = "http://dadosabertos.rio.rj.gov.br/apiTransporte/apresentacao/csv/onibus.cfm" ;
    private final static String RIO_BUS_BRT_URL = "http://dadosabertos.rio.rj.gov.br/apitransporte/apresentacao/csv/brt.cfm" ;

    final boolean isBRT ;

    public RioBusServicesFactory(boolean isBRT) {
        this.isBRT = isBRT;
    }

    /**
     * Creates a RioBusService that reads from RioBusRemoteDataProvider
     */
    @Override
    public Service createService() {
        return new RioBusService(new RioBusRemoteDataProvider(isBRT ? RIO_BUS_BRT_URL : RIO_BUS_URL ,createHttpClient())) ;
    }

    /**
     * Creates an instance of RioBusServiceStateOutput
     * @param writer where to write the output
     */
    @Override
    public ServiceStateOutput createServiceStateOutput(Writer writer) {
        return new RioBusServiceStateOutput(writer) ;
    }

    /**
     * Creates a default HttpClient to provide connectivity to RioBusRemoteDataProvider
     * @return
     */
    private static CloseableHttpClient createHttpClient() {
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(15 * 1000)
                .setSocketTimeout(20 * 1000)
                .setConnectTimeout(5 * 1000).build();
        return HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
    }
}
