package br.com.bguberfain;

import br.com.bguberfain.history.HistoryGenerator;
import br.com.bguberfain.services.Service;
import br.com.bguberfain.services.ServiceState;
import br.com.bguberfain.services.ServicesFactory;
import br.com.bguberfain.services.riobuses.RioBusServicesFactory;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;
import java.time.Month;
import java.util.Optional;

import static br.com.bguberfain.services.riobuses.RioBusTestUtils.rioBusSample;
import static br.com.bguberfain.services.riobuses.RioBusTestUtils.rioBusStateOf;
import static org.mockito.Mockito.* ;

/**
 * Created by Bruno on 07/06/2015.
 */
public class HistoryGeneratorTest {

    @Test
    public void testRun() throws Exception {
        // Hold output to StringWriter
        StringWriter output = new StringWriter() ;

        // Instantiate the class to test, with a mocked version of the Service
        HistoryGenerator historyGenerator = new HistoryGenerator(factoryMock(), 0, output) ;

        // Run the process
        historyGenerator.run(false, false, "regular-buses");

        String expectedOutput = "07-06-2015 13:41:50\tA01\tL23\t-43.232300\t-22.234000\t10.3\t100\n" +
                "07-06-2015 13:41:55\tA02\tL23\t-43.244300\t-22.853400\t50.1\t55\n" +
                "07-06-2015 13:43:30\tA02\tL23\t-43.544300\t-22.893400\t22.8\t10\n" ;

        // Assert correct output
        Assert.assertEquals(expectedOutput, output.toString());
    }

    /**
     * Creates a version of RioBusServicesFactory with a mock version of 'createService' method
     *
     * @return  The partial mock version of RioBusServicesFactory
     */
    private ServicesFactory factoryMock() {
        return new RioBusServicesFactory(false) {

            @Override
            public Service createService() {
                Service mockService = mock(Service.class) ;

                // First service return
                ServiceState return1 = rioBusStateOf(
                        rioBusSample(2015, Month.JUNE, 07, 13, 41, 50, "A01", "L23", -43.2323, -22.234, 10.3f, 100),
                        rioBusSample(2015, Month.JUNE, 07, 13, 41, 55, "A02", "L23", -43.2443, -22.8534, 50.1f, 55)
                ) ;

                // Second service return (only the second sample is different, even though the delay of the first one
                // has changed)
                ServiceState return2 = rioBusStateOf(
                        rioBusSample(2015, Month.JUNE, 07, 13, 41, 50, "A01", "L23", -43.2323, -22.234, 10.3f, 200),
                        rioBusSample(2015, Month.JUNE, 07, 13, 43, 30, "A02", "L23", -43.5443, -22.8934, 22.8f, 10)
                ) ;

                when(mockService.getCurrentState())
                        .thenReturn(Optional.of(return1))
                        .thenReturn(Optional.of(return2)) ;

                return mockService;
            }

        } ;
    }
}