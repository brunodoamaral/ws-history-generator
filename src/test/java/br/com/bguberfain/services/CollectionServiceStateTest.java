package br.com.bguberfain.services;

import br.com.bguberfain.services.riobuses.RioBusSample;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Bruno on 19/04/2015.
 */
public class CollectionServiceStateTest {

    /**
     * Test the subtraction of states
     */
    @Test
    public void testMinusSimpleObjects() {
        CollectionServiceState<String> obj1 = collectionOf("Value1", "Value2", "Value3") ;
        CollectionServiceState<String> obj2 = collectionOf("Value1", "Value3") ;

        CollectionServiceState<String> objDiff = (CollectionServiceState<String>) obj1.minus(obj2);

        Assert.assertNotSame(obj1, objDiff);
        Assert.assertNotSame(obj2, objDiff);
        Assert.assertEquals(objDiff.size(), 1);
        Assert.assertTrue(objDiff.contains("Value2"));
    }

    /**
     * Test an invalid operation
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testMinusWithInvalidParameter() {
        // Create an anonymous ServiceState
        ServiceState customState = otherState -> null ;

        CollectionServiceState<String> aCollection = collectionOf("Value1", "Value2", "Value3") ;

        aCollection.minus(customState) ;
    }

    /**
     * Test the subtraction of objects (must implement "equals" and "hashCode" correctly)
     */
    @Test
    public void testMinusComplexObjects() throws Exception {
        CollectionServiceState<RioBusSample> obj1 = collectionOf(
                new RioBusSample(new Date(1000), "A10020", "310", -22, -43, 10.0f, 60),
                new RioBusSample(new Date(2000), "A10021", "310", -22, -43, 20.0f, 120),
                new RioBusSample(new Date(2000), "A10023", "310", -22, -43, 30.0f, 180) ) ;

        CollectionServiceState<RioBusSample> obj2 = collectionOf(
                new RioBusSample(new Date(1000), "A10020", "310", -22, -43, 10.0f, 240),        // Equals to first of obj1
                new RioBusSample(new Date(2000), "A10023", "310", -22, -43, 30.0f, 300) ) ;     // Equals to last of obj1

        CollectionServiceState<String> objDiff = (CollectionServiceState<String>) obj1.minus(obj2);

        Assert.assertNotSame(obj1, objDiff);
        Assert.assertNotSame(obj2, objDiff);
        Assert.assertEquals(1, objDiff.size());
        Assert.assertTrue(objDiff.contains(new RioBusSample(new Date(2000), "A10021", "310", -22, -43, 20.0f, 0)));
    }

    /**
     * Utility method to return a CollectionServiceState from the provided objects
     *
     * @param objs
     * @param <T>
     * @return
     */
    @SafeVarargs
    private final <T extends Comparable> CollectionServiceState<T> collectionOf(T... objs) {
        Set<T> setOfObj = new HashSet<>(Arrays.asList(objs)) ;

        return new CollectionServiceState<>(setOfObj);
    }
}