package br.com.bguberfain.services.riobuses;

import br.com.bguberfain.services.riobuses.RioBusRemoteDataProvider;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicStatusLine;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Bruno on 06/06/2015.
 */
public class RioBusRemoteDataProviderTest {

    /**
     * Test an valid response from server (HttpStatus.SC_OK = 200), returning the string "any string" from its HttpEntity.getContent().
     *
     * @throws IOException (should not be thrown)
     */
    @Test
    public void testRemoteStateReader() throws IOException {
        // Configure HttpEntity
        String responseBuffer = "any string" ;
        HttpEntity httpEntity = mock(HttpEntity.class) ;
        when(httpEntity.getContent()).thenReturn(new ByteArrayInputStream(responseBuffer.getBytes())) ;

        // Configure OK response
        CloseableHttpResponse response = mock(CloseableHttpResponse.class) ;
        when(response.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "any")) ;
        when(response.getEntity()).thenReturn(httpEntity) ;

        // Configure return of response from httpClient.execute
        CloseableHttpClient httpClient = mock(CloseableHttpClient.class) ;
        when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response) ;

        RioBusRemoteDataProvider dataProvider = new RioBusRemoteDataProvider("http://any.url", httpClient) ;

        Optional<Reader> reader = dataProvider.remoteStateReader() ;

        // Check return
        assertTrue(reader.isPresent());

        String resultStream = new BufferedReader(reader.get()).readLine() ;
        assertEquals(responseBuffer, resultStream);
    }

    /**
     * Test an error response from server (HttpStatus.SC_INTERNAL_SERVER_ERROR = 500).
     * I this case, the returning state should be empty (Optional.isPresent() == false)
     *
     * @throws IOException (should not be thrown)
     */
    @Test
    public void testRemoteStateReaderError() throws IOException {
        // Configure non-OK response
        CloseableHttpResponse response = mock(CloseableHttpResponse.class) ;
        when(response.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_INTERNAL_SERVER_ERROR, "any")) ;

        // Configure return of response from httpClient.execute
        CloseableHttpClient httpClient = mock(CloseableHttpClient.class) ;
        when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response) ;

        RioBusRemoteDataProvider dataProvider = new RioBusRemoteDataProvider("http://any.url", httpClient) ;

        Optional<Reader> reader = dataProvider.remoteStateReader() ;

        // Check return
        assertFalse(reader.isPresent());
    }
}