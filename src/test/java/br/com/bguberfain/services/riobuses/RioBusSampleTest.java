package br.com.bguberfain.services.riobuses;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * Created by Bruno on 18/04/2015.
 */
public class RioBusSampleTest {

    /**
     * Test invalid null order
     * @throws InvalidRioBusState as expected
     */
    @Test(expected = InvalidRioBusState.class)
    public void testInvalidNullOrder() throws InvalidRioBusState {
        new RioBusSample(new Date(1000), null, "310", -22, -43, 20, 60) ;
    }

    /**
     * Test invalid empty order
     * @throws InvalidRioBusState as expected
     */
    @Test(expected = InvalidRioBusState.class)
    public void testInvalidEmptyOrder() throws InvalidRioBusState {
        new RioBusSample(new Date(1000), "", "310", -22, -43, 20, 60) ;
    }

    /**
     * Test invalid negative latitude (below -90)
     * @throws InvalidRioBusState as expected
     */
    @Test(expected = InvalidRioBusState.class)
    public void testInvalidNegativeLatitude() throws InvalidRioBusState {
        new RioBusSample(new Date(1000), "A12564", "310", -90.001, -43, 20, 60) ;
    }

    /**
     * Test invalid positive latitude (above 90)
     * @throws InvalidRioBusState as expected
     */
    @Test(expected = InvalidRioBusState.class)
    public void testInvalidPositiveLatitude() throws InvalidRioBusState {
        new RioBusSample(new Date(1000), "A12564", "310", +90.001, -43, 20, 60) ;
    }

    /**
     * Test invalid negative longitude (below -180)
     * @throws InvalidRioBusState as expected
     */
    @Test(expected = InvalidRioBusState.class)
    public void testInvalidNegativeLongitude() throws InvalidRioBusState {
        new RioBusSample(new Date(1000), "A12564", "310", -22, -180.0001, 20, 60) ;
    }

    /**
     * Test invalid positive longitude (above 180)
     * @throws InvalidRioBusState as expected
     */
    @Test(expected = InvalidRioBusState.class)
    public void testInvalidPositiveLongitude() throws InvalidRioBusState {
        new RioBusSample(new Date(1000), "A12564", "310", -22, +180.0001, 20, 60) ;
    }

    /**
     * Tests equals end hashCode method of RioBusSample. They must include all fields, except 'delay'
     * @throws InvalidRioBusState (should not be thrown)
     */
    @Test
    public void testEqualsAndHash() throws InvalidRioBusState {
        // Test equals
        RioBusSample validState1 = new RioBusSample(new Date(1000), "A12564", "310", -22, -43, 20, 60) ;
        RioBusSample validState2 = new RioBusSample(new Date(1000), "A12564", "310", -22, -43, 20, 60) ;

        Assert.assertEquals(validState1, validState2);
        Assert.assertEquals(validState1.hashCode(), validState2.hashCode());

        // Test difference on each field

        // Different timestamp:
        Assert.assertNotEquals(validState1, new RioBusSample(new Date(1001), "A12564", "310", -22, -43, 20, 60));
        Assert.assertNotEquals(validState1.hashCode(), new RioBusSample(new Date(1001), "A12564", "310", -22, -43, 20, 60).hashCode());

        // Different order:
        Assert.assertNotEquals(validState1, new RioBusSample(new Date(1000), "A12565", "310", -22, -43, 20, 60));
        Assert.assertNotEquals(validState1.hashCode(), new RioBusSample(new Date(1000), "A12565", "310", -22, -43, 20, 60).hashCode());

        // Different lineNumber:
        Assert.assertNotEquals(validState1, new RioBusSample(new Date(1000), "A12564", "311", -22, -43, 20, 60));
        Assert.assertNotEquals(validState1.hashCode(), new RioBusSample(new Date(1000), "A12564", "311", -22, -43, 20, 60).hashCode());

        // Different latitude:
        Assert.assertNotEquals(validState1, new RioBusSample(new Date(1000), "A12564", "310", -23, -43, 20, 60));
        Assert.assertNotEquals(validState1.hashCode(), new RioBusSample(new Date(1000), "A12564", "310", -23, -43, 20, 60).hashCode());

        // Different longitude:
        Assert.assertNotEquals(validState1, new RioBusSample(new Date(1000), "A12564", "310", -22, -44, 20, 60));
        Assert.assertNotEquals(validState1.hashCode(), new RioBusSample(new Date(1000), "A12564", "310", -22, -44, 20, 60).hashCode());

        // Different speed:
        Assert.assertNotEquals(validState1, new RioBusSample(new Date(1000), "A12564", "310", -22, -43, 21, 60));
        Assert.assertNotEquals(validState1.hashCode(), new RioBusSample(new Date(1000), "A12564", "310", -22, -43, 21, 60).hashCode());

        // Different delay -> this field is not considered on equals and hashCode:
        Assert.assertEquals(validState1, new RioBusSample(new Date(1000), "A12564", "310", -22, -43, 20, 61));
        Assert.assertEquals(validState1.hashCode(), new RioBusSample(new Date(1000), "A12564", "310", -22, -43, 20, 61).hashCode());
    }
}
