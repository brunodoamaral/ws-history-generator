package br.com.bguberfain.services.riobuses;

import br.com.bguberfain.services.CollectionServiceState;
import org.junit.Test;

import java.io.StringWriter;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertEquals;

/**
 * Created by Bruno on 06/06/2015.
 */
public class RioBusServiceStateOutputTest {

    /**
     * Test correct output of state. This output should be in English (ie, '.' as decimal separator).
     *
     * @throws Exception
     */
    @Test
    public void testAppendState() throws Exception {
        StringWriter stringWriter = new StringWriter() ;

        RioBusServiceStateOutput serviceOut = new RioBusServiceStateOutput(stringWriter) ;

        serviceOut.appendState(generateRioBusState());

        String currentString = stringWriter.getBuffer().toString() ;
        String expectedString = "12-06-2014 17:15:45\tO1\t220\t-43.654200\t-22.986100\t10.8\t100\n12-06-2014 17:17:15\tO1\t220\t-43.665500\t-22.990500\t5.2\t55\n" ;

        assertEquals(currentString, expectedString);

        serviceOut.appendState(generateRioBusState());

        currentString = stringWriter.getBuffer().toString() ;
        assertEquals(currentString, expectedString + expectedString);
    }

    /**
     * Internal method to create samples.
     *
     * @return Specific samples from 12/06/2014 17:15:45 and 12/06/2014 17:17:15
     * @throws InvalidRioBusState
     */
    private CollectionServiceState<RioBusSample> generateRioBusState() throws InvalidRioBusState {
        ZoneId tmzSaoPaulo = ZoneId.of("America/Sao_Paulo");

        ZonedDateTime firstSampleTime = ZonedDateTime.of(LocalDateTime.of(2014, Month.JUNE, 12, 17, 15, 45), tmzSaoPaulo) ;
        ZonedDateTime secondSampleTime = firstSampleTime.plus(90, ChronoUnit.SECONDS) ;

        CollectionServiceState<RioBusSample> state = new CollectionServiceState<>() ;

        state.add(new RioBusSample(Date.from(firstSampleTime.toInstant()), "O1", "220", -43.6542, -22.9861, 10.8f, 100)) ;
        state.add(new RioBusSample(Date.from(secondSampleTime.toInstant()), "O1", "220", -43.6655, -22.9905, 5.2f, 55)) ;

        return state ;
    }
}