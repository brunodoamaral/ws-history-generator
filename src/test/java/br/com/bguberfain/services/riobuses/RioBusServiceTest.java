package br.com.bguberfain.services.riobuses;

import br.com.bguberfain.services.CollectionServiceState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.Matches;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.time.Month;
import java.util.Optional;

import static br.com.bguberfain.services.riobuses.RioBusTestUtils.rioBusSample;
import static br.com.bguberfain.services.riobuses.RioBusTestUtils.rioBusStateOf;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Bruno on 06/06/2015.
 */
public class RioBusServiceTest {

    /**
     * Holds the original System.err information
     */
    private final ByteArrayOutputStream errOutput = new ByteArrayOutputStream() ;

    /**
     * Redirects the System.err to local 'errOutput'
     */
    @Before
    public void init() {
        // Redirect System.err to errOutput
        System.setErr(new PrintStream(errOutput));
    }

    /**
     * Revert System.err to original
     */
    @After
    public void destroy() {
        // Reset System.err
        System.setErr(System.err);
    }

    /**
     * Tests the RioBusService capability of transforming server states in local states.
     * Makes 3 calls:
     *      1. Return two regular samples
     *      2. Return no samples
     *      3. Return two other regular samples
     */
    @Test
    public void testRemoteDataParser() {
        // Prepare mock results states: [A,null,B]
        RioBusRemoteDataProvider mockDataProvider = mock(RioBusRemoteDataProvider.class) ;
        Optional<Reader> result1 = Optional.of(serverReturnA()) ;
        Optional<Reader> result2 = Optional.empty() ;
        Optional<Reader> result3 = Optional.of(serverReturnB()) ;
        when(mockDataProvider.remoteStateReader())
                .thenReturn(result1)
                .thenReturn(result2)
                .thenReturn(result3);


        // Instantiate class to test
        RioBusService rioBusService = new RioBusService(mockDataProvider) ;

        // Get first result
        Optional<CollectionServiceState<RioBusSample>> optResultState1 = rioBusService.getCurrentState() ;

        //// Test presence
        assertTrue(optResultState1.isPresent());

        //// Test equals
        CollectionServiceState<RioBusSample> expectedResult1 = rioBusStateOf(
                rioBusSample(2015, Month.JUNE, 6, 11, 55, 26, "B73080", "SPB350", -22.8971, -43.2025, 51, 0),
                rioBusSample(2015, Month.JUNE, 6, 11, 55, 26, "B51509", "SV917", -22.8868, -43.304, 34, 0)
        );
        assertEquals(expectedResult1, optResultState1.get());


        // Get second result
        Optional<CollectionServiceState<RioBusSample>> optResultState2 = rioBusService.getCurrentState() ;

        //// Test not present
        assertFalse(optResultState2.isPresent());


        // Get third result
        Optional<CollectionServiceState<RioBusSample>> optResultState3 = rioBusService.getCurrentState() ;

        //// Test presence
        assertTrue(optResultState3.isPresent());

        //// Test equals
        CollectionServiceState<RioBusSample> expectedResult3 = rioBusStateOf(
                rioBusSample(2015, Month.JUNE, 6, 11, 56, 56, "B73080", "SPB350", -22.8938, -43.1957, 28, 0),
                rioBusSample(2015, Month.JUNE, 6, 11, 57, 16, "B51509", "SV917", -22.8857, -43.309, 33, 0)
        );
        assertEquals(expectedResult3, optResultState3.get());
    }

    /**
     * Tests whatever an invalid date is presented.
     * Checks the log for stack trace mark
     */
    @Test
    public void testInvalidDate() {
        // Prepare mock results states
        RioBusRemoteDataProvider mockDataProvider = mock(RioBusRemoteDataProvider.class);
        Optional<Reader> invalidResult = Optional.of(serverReturnInvalidDate());
        when(mockDataProvider.remoteStateReader()).thenReturn(invalidResult);


        // Instantiate class to test
        RioBusService rioBusService = new RioBusService(mockDataProvider);

        // Reset error buffer
        errOutput.reset() ;

        // Get first result
        Optional<CollectionServiceState<RioBusSample>> optResultState1 = rioBusService.getCurrentState();

        // Test not presence
        assertFalse(optResultState1.isPresent());

        // Assert error output
        assertThat(firstErrorLine(), new Matches(".*java\\.text\\.ParseException.*2015\\-06\\-06 11:56:56.*"));
    }

    /**
     * Tests whatever an invalid bus data is presented (in this case, an invalid latitude).
     * Checks the log for stack trace mark
     */
    @Test
    public void testInvalidBusData()
    {
        // Prepare mock results states
        RioBusRemoteDataProvider mockDataProvider = mock(RioBusRemoteDataProvider.class) ;
        Optional<Reader> result1 = Optional.of(serverReturnOneInvalidBus()) ;
        when(mockDataProvider.remoteStateReader()).thenReturn(result1);


        // Instantiate class to test
        RioBusService rioBusService = new RioBusService(mockDataProvider) ;

        // Reset error buffer
        errOutput.reset() ;

        // Get first result
        Optional<CollectionServiceState<RioBusSample>> optResultState1 = rioBusService.getCurrentState() ;

        // Test presence
        assertTrue(optResultState1.isPresent());

        // Test equals
        CollectionServiceState<RioBusSample> expectedResult1 = rioBusStateOf(
                rioBusSample(2015, Month.JUNE, 6, 11, 55, 26, "B73080", "SPB350", -22.8971, -43.2025, 51, 0)
        );
        assertEquals(expectedResult1, optResultState1.get());

        // Assert error output
        assertThat(firstErrorLine(), new Matches(".*br\\.com\\.bguberfain\\.services\\.riobuses\\.InvalidRioBusState.*"));
    }

    /**
     * Creates and returns a Reader as returned by the actual server
     * @return a server state with two valid samples
     */
    private Reader serverReturnA() {
        return new StringReader(
                "dataHora,ordem,linha,latitude,longitude,velocidade\n" +
                "06-06-2015 11:55:26,B51509,SV917,\"-22.8868\",\"-43.304\",34\n" +
                "06-06-2015 11:55:26,B73080,SPB350,\"-22.8971\",\"-43.2025\",51") ;
    }

    /**
     * Creates and returns a Reader as returned by the actual server
     * @return a server state with two other valid samples
     */
    private Reader serverReturnB() {
        return new StringReader(
                "dataHora,ordem,linha,latitude,longitude,velocidade\n" +
                "06-06-2015 11:56:56,B73080,SPB350,\"-22.8938\",\"-43.1957\",28\n" +
                "06-06-2015 11:57:16,B51509,SV917,\"-22.8857\",\"-43.309\",33") ;
    }

    /**
     * Creates and returns a Reader as returned by the actual server, but with an invalid date (yyyy-MM-dd
     * instead of dd-MM-yyyy)
     * @return a server state with one invalid sample
     */
    private Reader serverReturnInvalidDate() {
        return new StringReader(
                "dataHora,ordem,linha,latitude,longitude,velocidade\n" +
                "2015-06-06 11:56:56,B73080,SPB350,\"-22.8938\",\"-43.1957\",28") ;
    }

    /**
     * Creates and returns a Reader as returned by the actual server, but with an invalid latitude (below -180)
     * @return a server state with one invalid sample
     */
    private Reader serverReturnOneInvalidBus() {
        return new StringReader(
                "dataHora,ordem,linha,latitude,longitude,velocidade\n" +
                "06-06-2015 11:55:26,B51509,SV917,\"-222.8868\",\"-43.304\",34\n" + // Latitude -222.8868 is invalid
                "06-06-2015 11:55:26,B73080,SPB350,\"-22.8971\",\"-43.2025\",51") ;
    }

    /**
     * Gather the first line of errOutput
     * @return The first line of errOutput
     */
    private String firstErrorLine() {
        String allError = errOutput.toString() ;
        int idxNewLine = allError.indexOf('\n') ;
        if (idxNewLine >= 0) {
            return allError.substring(0, idxNewLine-1) ;
        } else {
            return allError ;
        }
    }
}