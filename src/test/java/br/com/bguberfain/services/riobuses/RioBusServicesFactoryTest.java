package br.com.bguberfain.services.riobuses;

import br.com.bguberfain.services.Service;
import br.com.bguberfain.services.ServiceStateOutput;
import br.com.bguberfain.services.ServicesFactory;
import org.junit.Test;

import java.io.Writer;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
/**
 * Created by Bruno on 07/06/2015.
 *
 * Tests instance returned by RioBusServicesFactory
 */
public class RioBusServicesFactoryTest {

    /**
     * Test Service creation
     */
    @Test
    public void testCreateService() {
        ServicesFactory factory = new RioBusServicesFactory(false) ;
        Service service = factory.createService() ;

        assertThat(service, instanceOf(RioBusService.class));
    }

    /**
     * Test ServiceStateOutput creation
     */
    @Test
    public void testCreateServiceStateOutput() {
        ServicesFactory factory = new RioBusServicesFactory(false) ;
        ServiceStateOutput serviceOut = factory.createServiceStateOutput( mock(Writer.class) ) ;

        assertThat(serviceOut, instanceOf(RioBusServiceStateOutput.class));

    }
}