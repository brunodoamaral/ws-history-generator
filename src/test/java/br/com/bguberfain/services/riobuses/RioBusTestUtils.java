package br.com.bguberfain.services.riobuses;

import br.com.bguberfain.services.CollectionServiceState;
import org.junit.Ignore;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;

/**
 * Created by Bruno on 07/06/2015.
 */
public class RioBusTestUtils {

    /**
     * Sugar core to instantiate CollectionServiceState
     * @param samples samples to be in CollectionServiceState
     * @return
     */
    @Ignore
    public static CollectionServiceState<RioBusSample> rioBusStateOf(RioBusSample ... samples) {
        CollectionServiceState<RioBusSample> result = new CollectionServiceState<>();

        Collections.addAll(result, samples);

        return result ;
    }

    /**
     * Sugar code to create an RioBusSample at S�o Paulo's time zone
     *
     * @return the sample created
     */
    @Ignore
    public static RioBusSample rioBusSample( int year, Month month, int day, int hour, int minute, int second, String order, String lineNumber, double lat, double lon, float speed, long delay) {
        ZoneId tmzSaoPaulo = ZoneId.of("America/Sao_Paulo");

        ZonedDateTime sampleTime = ZonedDateTime.of(LocalDateTime.of(year, month, day, hour, minute, second), tmzSaoPaulo) ;

        try {
            return new RioBusSample(Date.from(sampleTime.toInstant()), order, lineNumber, lat, lon, speed, delay) ;
        } catch (InvalidRioBusState invalidRioBusState) {
            invalidRioBusState.printStackTrace();
            return null ;
        }
    }
}
